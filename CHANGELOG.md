# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.5]
### Added
- Changelog Initialized

[0.1.5]: https://gitlab.com/hxr-io/wgns/-/tree/v0.1.5
[0.1.4]: https://gitlab.com/hxr-io/wgns/-/tree/v0.1.4
[0.1.3]: https://gitlab.com/hxr-io/wgns/-/tree/v0.1.3
[0.1.2]: https://gitlab.com/hxr-io/wgns/-/tree/v0.1.2
[0.1.1]: https://gitlab.com/hxr-io/wgns/-/tree/v0.1.1
[Unreleased]: https://gitlab.com/hxr-io/wgns/-/tree/main
