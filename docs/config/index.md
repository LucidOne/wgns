---
title: Configuration
---

# Setup
## Generate local keys
``` bash
RESET_UMASK=$(umask -p) \
&& umask 077 \
&& wg genkey | tee -a ~/.config/wgns/wgvpn0.key | wg pubkey > ~/.config/wgns/wgvpn0.pub \
&& $RESET_UMASK
```

## Configure `wgns`
Most configuration options can be passed via environmental variables. Variables configured in `$XDG_CONFIG_HOME/wgns/$WGNS_WG_INTERFACE.env` will be loaded automatically.

### Wifi Example
``` bash title="~/.config/wgns/wgvpn0.env"
--8<-- "docs/config/wgvpn0.env"
```

### Ethernet Example
``` bash title="~/.config/wgns/wgvpn0.env"
#!/usr/bin/env bash
WGNS_INTERFACE="enp0s0f0"
WGNS_INTERFACE_TYPE="ethernet"
WGNS_IP="172.16.15.14/12"
WGNS_DNS="172.16.15.2"
```

## Configure WireGuard
The WireGuard configuration is loaded via `wg setconf $WGNS_WG_INTERFACE ${WGNS_CONFIG_WG_VPN}` so any options that makes sense in that context should work.

``` toml title="~/.config/wgns/wgvpn0.conf"
--8<-- "docs/config/wgvpn0.conf"
```

## Configure WiFi
When `WGNS_INTERFACE_TYPE="wifi"` an instance of `wpa_supplicant` is started by `wgns` inside the network namespace to manage wifi connections, this approach also conveniently prevents ownership conflicts with NetworkManager and systemd.

``` title="~/.config/wgns/wpa_supplicant.conf"
--8<-- "docs/config/wpa_supplicant.conf"
```

Multiple wifi networks can be listed and setting `scan_ssid=0` in `wpa_supplicant.conf` is helpful if you would prefer not broadcasting you home SSID at work.

# Notes, caveats, and warnings
## Troubleshooting Namespace issues
It's easier than you might expect to debug namespace issues, just run the following to open a shell inside the namespace!

``` bash
$ sudo ip netns exec $WGNS_NAMESPACE bash
$ ip a
```
