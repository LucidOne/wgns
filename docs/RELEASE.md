# Release Process
## Tag Release & Push
``` bash
git tag v0.1.4
git push origin v0.1.4
```

## Switch to Ubuntu release branch
``` bash
git checkout ubuntu/jammy
```

## Sync `.gitlab-ci.yml` from `main` if necessary
``` bash
git checkout main .gitlab-ci.yml
git reset
git diff
git commit .gitlab-ci.yml
git push
```

## Verify `debian/*`, Tag Ubuntu Release & Push
```
git tag ubuntu/jammy/v0.1.4
git push origin ubuntu/jammy/v0.1.4
```

## Post release
Check [Launchpad](https://launchpad.net/~hxr-io/+archive/ubuntu/aesthetic/+packages) to verify that the package has been `Published`.

After the package is available, start the blocked `jammy-ppa` to verify that the package is installable from the PPA.
