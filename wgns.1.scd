WGNS(1) "wgns" "WireGuard NameSpace controller"

# NAME

wgns - up/down controller for running WireGuard in a namespace

# SYNOPSIS

*wgns* _up_ [_WG_INTERFACE_]

*wgns* _down_ [_WG_INTERFACE_]

# DESCRIPTION

A cli tool for starting and stopping WireGuard connections in a namespace.

# ENVIRONMENT VARIABLES

WGNS_WG_INTERFACE
	The WireGuard network interface, which defaults to _wgvpn0_.

WGNS_CONFIG_ENV
	The wgns configuration, nominally `~/.config/wgns/$WGNS_WG_INTERFACE.env`.

WGNS_IP
	local IP inside the WireGuard network, generally configured in $WGNS_CONFIG_ENV.

WGNS_DNS
	DNS server inside the WireGuard network, often configured in $WGNS_CONFIG_ENV.

WGNS_CONFIG_WPA
	Config file for WPA Supplicant, this defaults to `~/.config/wgns/wpa_supplicant.conf`. Passwords for access points can be specified here.

WGNS_CONFIG_WG_VPN
	The config file for WireGuard itself, by default `~/.config/wgns/$WGNS_WG_INTERFACE.conf`

WGNS_NAMESPACE
	The namespace wireguard and the physical network device are assigned to, by default `wireguard`

WGNS_INTERFACE
	The physical network interface, by default `wlan0`

WGNS_INTERFACE_TYPE
	The physical network interface type ['wifi','ethernet'], with the default of `wifi`.

WGNS_INTERFACE_PHY
	This is used to assign the wifi interface to the namespace, by default `phy0`.

# SEE ALSO

_wg_(8),
<https://hxr-io.gitlab.io/wgns/>,
<https://gitlab.com/hxr-io/wgns>

# AUTHORS

HXR <code@hxr.io>
