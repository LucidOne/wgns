#syntax=docker/dockerfile:1.6
################################################################
#
FROM alpine:latest
MAINTAINER HXR <code@hxr.io>

ARG CI=false
ARG CI_COMMIT_SHA=undefined
ARG CONTAINER_IMAGE=ci-ansible
ARG CONTAINER_VERSION=unknown
ARG METADATA=/var/opt/container/metadata

ENV WG_FIXTURE_IP=10.12.34.1/24
ENV NETCAT_FIXTURE_PORT=1234

RUN apk add --no-cache wireguard-tools-wg netcat-openbsd tini-static gettext iproute2

COPY docker-entrypoint.sh /usr/local/bin/
COPY wg0.conf.template /usr/local/share/wgns/

RUN mkdir -p "$(dirname "${METADATA}")" \
    && echo "Writing container metadata to ${METADATA}" \
    && printf "CI=%s\n" "${CI}" >> "${METADATA}" \
    && printf "CI_COMMIT_SHA=%s\n" "${CI_COMMIT_SHA}" >> "${METADATA}" \
    && printf "CONTAINER_IMAGE=%s\n" "${CONTAINER_IMAGE}" >> "${METADATA}" \
    && printf "CONTAINER_VERSION=%s\n" "${CONTAINER_VERSION}" >> "${METADATA}" \
    && cat "${METADATA}"

EXPOSE $NETCAT_FIXTURE_PORT
EXPOSE 51820

ENTRYPOINT ["/sbin/tini-static", "--"]
CMD ["/usr/local/bin/docker-entrypoint.sh"]
