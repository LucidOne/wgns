#shellcheck shell=sh
Describe 'wgns'
  Describe 'cli'
    It 'outputs usage'
      When run script wgns
      The error should start with 'Usage:'
      The error should end with 'wgns up|down [INTERFACE]'
      The status should equal 64
    End
  End
End
